<?php

namespace Database\Factories;

use App\Models\HodLogin;
use Illuminate\Database\Eloquent\Factories\Factory;

class HodLoginFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = HodLogin::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
