<?php

namespace Database\Factories;

use App\Models\TeacherLogin;
use Illuminate\Database\Eloquent\Factories\Factory;

class TeacherLoginFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TeacherLogin::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
