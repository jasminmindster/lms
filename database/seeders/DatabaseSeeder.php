<?php

namespace Database\Seeders;

use App\Models\Hod;
use App\Models\Teacher;
use App\Models\HodLogin;
use App\Models\Department;
use App\Models\TeacherLogin;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        //$this->truncateModels();

       // $this->call(DepartmentSeeder::class);

       Teacher::factory(5)->create();
       TeacherLogin::factory(5)->create();
       Hod::factory(5)->create();
       HodLogin::factory(5)->create();

    }

    
    private function truncateModels()
    {
        $models = [
            Teacher::class,
            Hod::class,
            HodLogin::class,
            TeacherLogin::class,
            Department::class,
        ];

        foreach ($models as $model) {
            call_user_func($model . '::truncate');
        }
    }
}
