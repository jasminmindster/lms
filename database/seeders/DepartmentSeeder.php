<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Computer Science and Engineering',
            ],
            [ 
                'name' => 'Civil Engineering',
            ],
            [
                'name' => 'Mechanical Engineering',
            ],
            [
                'name' => 'Electrical and Electronics Engineering',
            ],
            [
                'name' => 'Electronics Engineering',
            ],
        ];
        
        DB::table('departments')->insert($data);
    }
}
